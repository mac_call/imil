# C from scratch: Épisode 1

**7 octobre 2021** 

<img src="images/cfs_001/01.png" alt="" width="300"/>

- ![youtube](../images/youtube.png) Youtube : https://www.youtube.com/watch?v=rYHmlKj6EUE




# Introduction 
[![youtube](../images/youtube.png)](https://www.youtube.com/watch?v=rYHmlKj6EUE&t=11m10s) (00:11:10)

Pour connaitre l'histoire d'Unix (BSD Unix) et de l'internet :
- ![youtube](../images/youtube.png) [Histoire de BSD unix 1 (durée 1h08)](https://www.youtube.com/watch?v=M-9flcXwwjI)
- ![youtube](../images/youtube.png) [Histoire de BSD unix 2 ( durée 1h15)](https://www.youtube.com/watch?v=96onZV_hUfs)



## Historique : C le langage de la vie

L'histoire de C est intimement liée avec l'histoire d'Unix...

Dans les années 70, deux <strike>personnes</strike> légendes  travaillent sur un système d'exploitation multi-tâches et multi-utilisateurs : Unix.

Ken Thompson et Dennis Ritchie travaillent sur un mini-ordinateur de l'époque : le PDP7.

Unix est écrit en assembleur. L'assembleur est le langage du processeur composé d'instructions simples : 
 - déplacer une valeur dans un registre (une "case" processeur)
 - additionner 2 registres
 - comparer 2 valeurs
 - ...

Avec ce genre de langage, faire des choses de "haut niveau", c'est long. De plus chaque type de processeur a son propre "assembleur" différent.  
Quand on veut "porter" une application d'un système à l'autre, il faut se farcir toute la conversion (les mots sont différents, les instructions sont différentes).

En 1970-1971, Unix existe en assembleur pour le PDP7

Thompson et Ritchie arrivent à faire venir un PDP11, aux Bell Labs, où ils travaillent. Le PDP11 est plus puissant, mais il faut de nouveau convertir unix pour cette nouvelle machine.  
Ken Thompson émet le souhait d'avoir un langage de plus haut niveau, qui pourrait s'abstraire des différences entre les machines.

Il existait déjà le B [![wikipedia](../images/wikipedia.png)](https://fr.wikipedia.org/wiki/B_(langage)), lui même dérivé du BPCL.  
Le B était limité, Thompson demande alors à Dennis Ritchie de travailler sur un nouveau langage. Ce sera le C.

Dennis Ritchie [![wikipedia](../images/wikipedia.png)](https://fr.wikipedia.org/wiki/Dennis_Ritchie) est l'inventeur du langage C, co-inventeur d'unix.  
Avec Ken Thompson   [![wikipedia](../images/wikipedia.png)](https://fr.wikipedia.org/wiki/Ken_Thompson), ils ont inventés des concepts encore utilisés aujourd'hui.

Les notions manipulées en C on été créées en premier pour Unix.

Interview de Ken Thompson par Brian Kernighan (le K du célèbre K&R, la bible du C... le R étant Ritchie) : [![youtube](../images/youtube.png)](https://youtube.com/watch?v=EY6q5dv_B-o)  

Le C vient d'un besoin  de faciliter la portabilité d'Unix sur plusieurs types de machines.

Le langage machine est le plus proche du CPU, mais le langage à l'origine même de tout ce qu'on fait, de l'internet, et de la vie :) c'est le C.

## Ce qui vous attend ici

Le C permet, certes, de faire beaucoup de bêtises de par son "bas niveau". Mais dans cette série, on va voir comment en faire le moins possible.  

Cette série de vidéos est destinée aux personnes sans forcément de connaissances "low level" mais voulant en savoir plus sur comment fonctionne un ordinateur.  

Ce ne sera pas un cours sur "comment faire une boucle, etc" mais plus sur "comprendre  comment ça marche sous unix".


# Le C et les fonctions.
[![youtube](../images/youtube.png)](https://www.youtube.com/watch?v=rYHmlKj6EUE&t=30m23s) (00:30:23)

Le C utilise des "fonctions". Les fonctions ne sont pas spécifiques au C, il y en a dans le shell par exemple :

```bash
int2hex() {printf "%x" $1 }

int2hex 255
ff%
```

- Sur la première ligne, on définit une fonction nommée `int2hex`. Les parenthèses après son nom indique que c'est une fonction.  le code devant être exécuté par cette fonction est écrit ensuite entre {}
- la 2ême ligne appelle la fonction en lui passant 255 en  paramètre
- la troisième ligne est le résultat de la fonction. La fonction est censé écrire à l'écran (printf) le code hexadécimal ("%x") du premier paramètre qui lui est passé à l'appel ($1). Pour 255 le résultat est ff%.

Notre fonction utilise un binaire : printf. La commande `which` du shell permet d'avoir des informations dessus. 

En zsh, `printf` est "built-in", c'est à dire intégré au shell.

```bash 
╰─ which printf
printf: shell built-in command

```

En bash c'est un programme externe (/usr/bin/printf)

```bash
which printf
/usr/bin/printf

```

# le C et les bibliothèques (libraries)
[![youtube](../images/youtube.png)](https://www.youtube.com/watch?v=rYHmlKj6EUE&t=34m39s) (00:34:39)

En C (comme dans d'autre langages comme le python par exemple) les fonctions sont regroupées en bibliothèques (libraries en anglais, le terme librairies est souvent employé en français).   
Une lib importante qui contient toute les fonctions de base du langage C est la libc.

sous linux ont peut la trouver dans `/lib/x86_64-linux-gnu/libc.so.6` :
```bash 
╰─ ls /lib/x86_64-linux-gnu/libc.so.6
/lib/x86_64-linux-gnu/libc.so.6

╰─ grep printf  /lib/x86_64-linux-gnu/libc.so.6
grep: /lib/x86_64-linux-gnu/libc.so.6 : fichiers binaires correspondent
```

Ci-dessus, le grep va chercher si "printf" est bien dans le fichier de la libc. "Fichiers binaires correspondent"  indique que la chaine "printf" est bien incluse dans le binaire de la libc (match).

Les fichiers .so sont des "Shared Objects"

Contrairement au python par exemple, en C on n' "importe" pas une lib, on "inclue" les headers (entêtes de descriptions) de la lib. On indique ainsi au compilateur : "ne t’inquiète pas, cette fonction qui s'utilise comme ça, tu l'auras en temps voulu (elle te sera liée/linkée)"

Par défaut les headers sont dans `/usr/include/`  
On y trouve `stdio.h` :
```bash
ls /usr/include/std*
stdc-predef.h  stdint.h  stdio.h  stdio_ext.h  stdlib.h
```

`stdio.h` contient des "prototypes" de fonction.  
par exemple, il définit comment on est censé utiliser  `printf` (paramètre en entrée, type de retour).  

Cette "déclaration" de la fonction, c'est déjà du C

```C
#include <stdio.h>
```

Le syntaxe ci-dessus, à positionner en haut de notre fichier `.c`, indique au compilateur (plus précisément au préprocesseur) d'inclure, dans notre code, le fichier `stdio.h`  qui contient les entêtes des fonctions standards du C traitant des input/output. Notre code C "connait" ainsi les fonctions stdio comme `printf` par exemple, et le compilateur n'y trouve plus rien à redire car il sait comment utiliser `printf` et peut donc vérifier que tout est fait correctement.  
Une autre façon de faire serait de définir dans notre fichier, uniquement le prototype de la fonction `printf` (si on n'utilise qu'elle). Avec beaucoup de fonctions à "include", cette façon de faire est fastidieuse, peu lisible et peu maintenable.


# Compiler
[![youtube](../images/youtube.png)](https://www.youtube.com/watch?v=rYHmlKj6EUE&t=48m20s) (00:48:20)

La compilation crée un binaire composé de langage machine qui peut être exécuté par le système d'exploitation.  

Un programme C compilé est une fonction. Il est géré par la libc et l'OS comme une fonction. Ce que cherche le linker (ld), pour le lancer, est sa fonction "main". main n'est donc pas a proprement parler le "point d'entrée" du programme.

Voir les "Linux (Vraiment) From Scratch" sur le format ELF pour plus de détail.
 - [![youtube](../images/youtube.png) L(v)FS S01E15 : Les ELFes des bois](https://www.youtube.com/watch?v=ycJt4h7pUzE)
 - [![youtube](../images/youtube.png) L(v)FS S01E16 : Relations ELFesteuses](https://www.youtube.com/watch?v=BRE_KUhIGm4)

Au final, le programme compilé sera, lui, "lié" à la libc (qui contient entre autres les fonctions décrites par stdio.h, et notamment `printf`). Les fichiers ELF exécutables sont "liés" aux différentes bibliothèques externes nécessaires au programme.  A l'exécution, les fonctions externes pourront donc être chargées en mémoire, sans faire réellement partie de notre binaire, mais étant installées sur la machine.


# Un peu de C
[![youtube](../images/youtube.png)](https://www.youtube.com/watch?v=rYHmlKj6EUE&t=53m27s) (00:53:27)

## Ouverture/création d'un fichier `0.c`
```bash
vim 0.c
```
## Contenu du fichier `0.c`
```c
int                  // ← type de retour de la fonction (le C est un langage typé)
main()               // ← main : nom de la fonction  
                     //   ()   : variables contenant les paramètres reçus par la fonction (aucune ici)
{
    return 0;        // ← retour de la valeur 0, signifie qu'il n'y a pas d'erreurs (0 = OK)
}
```

le "int" comme type de retour de la fonction main est implicite et "pourrait" être omis si on écrivait du code de cochon.

Autre chose à ne JAMAIS faire : `void main () {}`
Bien qu'accepté par le compilateur, ici, le main ne renvoie pas de code de retour (void), et la valeur reçue par le programme appelant est "indéterminée" (voir la dernière partie de la vidéo pour tester une hypothèse sur le sujet).  
**Un programme doit TOUJOURS renvoyer un code de retour consistant (0 si OK, autre si erreur), et pas au petit bonheur la chance.**



## Compilation de `0.c`
```bash
cc -o 0 0.c
```

## Lancement :
```bash
./0

```
On ne voit rien car le programme ne fait pas grand chose, se contenant de retourner 0 = tout va bien.  
Le code retour peut être tester avec `&&` en shell. La commande suivant le `&&` ne sera exécutée que si la commande précédent le `&&` retourne 0 (d'autres valeurs signifient "erreur", ou "false")

Lancement :
```bash
./0 && echo "booya"
booya

```
la commande echo écrit "booya" à l'écran car le programme "0"  a retourné 0 

Maintenant si on modifie notre fichier `0.c` en retournant 1 au lieu de 0. On n'oublie pas de recompiler, et on obtient :
Lancement :
```bash
./0 && echo "booya"


``` 
"booya" n'est plus affiché, "0" ayant retourné un code d'erreur.

## Voir les librairies liées
`ldd mon_prog` : liste les libs liées à l'exécutable "mon_prog". 

```bash
ldd 0
    linux-vdso.so.1 (0x00007fffe2d55000)
    libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fafb5d9b000)
    /lib64/ld-linux-x86-64.so.2 (0x00007fafb5ff1000)
```
La libc est bien liée a notre programme "0"



## exit() vs return
il est possible  de mettre `exit(0);` à la place de `return 0;`. Il faut alors inclure `<stdlib.h>`.

`exit 0` ne va pas nettoyer la pile (stack) derrière lui; contrairement à `return 0;` il vaut donc mieux utiliser return que exit sur une fin de programme. 
l'exemple typique d'un exit  est une fonction "--help" qui écrit le "usage" du programme : rien n'a été fait, on print le usage et on exit.


# Le préprocesseur
[![youtube](../images/youtube.png)](https://www.youtube.com/watch?v=rYHmlKj6EUE&t=1h18m25s) (01:18:25)


```c
int
main()
{
    return MONDEFINE;
}
```
```bash
cc -o 0 -DMONDEFINE=1 0.c
```

ici on fournie, au moment de la compilation, une valeur a MONDEFINE, valeur qui sera placée a la place de MONDEFINE dans le code.

**De manière équivalente :**  
il est possible de définir MONDEFINE par un #define dans le code:
```c
#define MONDEFINE 1
int
main()
{
    return MONDEFINE;
}
```
```bash
cc -o 0  0.c
```


Le préprocesseur ne fait que de "remplacer" nos include/define par le contenu qu'ils pointent (similaire au fonctionnement de `sed`).


`cc -E mon_prog.c` : lance une compilation de mon_prog.c, mais arrête la compilation après le pré-processeur, et liste le code source obtenu.  Si mon_prog.c inclue, au départ, des headers comme stdio.h, le code affiché par la commande ici contiendra tout le contenu de stdio.h, en plus de notre code ensuite.   



# Bonus : quel est le code de retour en cas de `void main()`
[![youtube](../images/youtube.png)](https://www.youtube.com/watch?v=rYHmlKj6EUE&t=1h25m42s) (01:25:42)

_**viewer discrétion is advised** : Assembleur   
Cette partie de la vidéo utilise des notions "avancées", elle ne doit être regardée que par des professionnels ! Les autres bouchez vous les yeux et les oreilles !  
( Non en vrai c'est technique mais intéressant. )_


L'hypothèse est que si le type de retour de main est void, le code de retour du programme est la dernière valeur du registre eax.

```bash
vim eax.c
```
```c
int geteax()
{
    __asm__("mov eax,42;");
}

void
main()
{
    geteax();
}
```

```bash
cc -o eax -masm=intel eax.c

./eax 

echo $?
42
```

Le programme `eax.c` possède un main qui return void (oouuh c'est mal) et qui apelle une fonction geteax().  
Cette fonction met la valeur 42 dans le registre eax du processeur (c'est fait en apellant une commande en assembleur qui 'move' 42 dans eax).  

On compile le programme en indiquant quel type de processeur est utilisé (l'assembleur dépend du processeur)

On lance le programme `eax.c`, puis on écrit le "code retour du programme précédent" ( echo de la variable shell $?)

le code de retour de notre programme est bien 42, qu'on avait posé dans eax... l'hypothèse est valide.

