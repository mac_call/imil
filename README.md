# Transcript/prise de notes  de certains épisodes  ...From Scratch de iMil

- ![Logo twitch](images/twitch.png) Chaine twitch : [https://www.twitch.tv/imilnb](https://www.twitch.tv/imilnb)
- ![Logo twitch](images/youtube.png) Chaine youtube (replay) :  https://www.youtube.com/channel/UCqGZmYTiWmOl7vXb0XBbY_Q


 
## Épisodes "C from scratch"

Le C de zéro, mais du C rigolo.

- [Playlist youtube](https://www.youtube.com/watch?v=rYHmlKj6EUE&list=PLo1MmTvqMbiUcyrms0_XPtVKYsvT-4y81)

### C from scratch : épisode 1

[<img src="c_from_scratch/images/cfs_001/01.png" alt="CFS épisode 1" width="250"/>](c_from_scratch/cfs_ep001.md)